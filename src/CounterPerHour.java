import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;

public class CounterPerHour {

    private List<LocalDateTime> count = new ArrayList<LocalDateTime>();

    public void add() {

    	try{
            this.clearUp();
            this.count.add(LocalDateTime.now());
		}catch(Throwable e){
        	e.printStackTrace();
			Log.log(Level.FINEST, "An exception was thrown", e);
		}

    }

    private void clearUp() {
    	try{
	        for (LocalDateTime i : this.count) {
	            if (i.compareTo(LocalDateTime.now().minusHours(1)) < 0) {
	                count.remove(i);
	            }
	        }
		}catch(Throwable e){
        	e.printStackTrace();
			Log.log(Level.FINEST, "An exception was thrown", e);
		}
    }

    
    public long getPerHour() {
        long result = -1;
        
	        try{
	            this.clearUp();

	            LocalDateTime latest = LocalDateTime.now();
	            for (LocalDateTime i : this.count) {
	                if (i.compareTo(latest) < 0) {
	                    latest = i;
	                }
	            }
	
	            long milliseconds = latest.until(LocalDateTime.now(), ChronoUnit.MILLIS);
	            double seconds = (double) count.size() / (double) milliseconds * 1000.0;
	            result = (long) (seconds * 60.0 * 60.0);
	
		}catch(Throwable e){
	    	e.printStackTrace();
			Log.log(Level.FINEST, "An exception was thrown", e);
		}
            
            return result;

    }

    public void reset() {
            count.clear();
    }
    
    
    //Test after this line
    public static void main(String[] args){
		// no comment NativeInterface.open must be the first instruction

		try{
			
			CounterPerHour c = new CounterPerHour();
			
			c.TestFill();
			c.TestGet();
			

		}catch(Throwable e){
        	e.printStackTrace();
			Log.log(Level.FINEST, "An exception was thrown", e);
		}
	}
	
	private void TestFill(){
		Log.log(Level.FINEST, "Counter.fill: start");

		try{
			
			Log.log(Level.FINEST, "Counter.get: test method");
			
			ExecutorService executor = Executors.newCachedThreadPool();
		    Runnable r1 = new Runnable() {
		        public void run() {
		        	try{
			        	for(int i=0;i<600;){
			        		Log.log(Level.FINEST, "Counter.fill: count:"+count);
			        		Thread.sleep(1000);
			        		add();
			        	}
					}catch(Throwable e){
			        	e.printStackTrace();
						Log.log(Level.FINEST, "An exception was thrown", e);
					}
		        }
		    };
		    executor.submit(r1);
			
			
			
		}catch(Throwable e){
        	e.printStackTrace();
			Log.log(Level.FINEST, "An exception was thrown", e);
		}
	}
	
	private void TestGet(){
		Log.log(Level.FINEST, "Counter.get: start");

		try{
			
			Log.log(Level.FINEST, "Counter.get: test method");
			
			ExecutorService executor = Executors.newCachedThreadPool();
		    Runnable r1 = new Runnable() {
		        public void run() {
		        	try{
			        	for(int i=0;i<60;){
			        		Thread.sleep(2000);
			        		Log.log(Level.FINEST, "Counter.fill: getPerHour:"+getPerHour());
			        	}
					}catch(Throwable e){
			        	e.printStackTrace();
						Log.log(Level.FINEST, "An exception was thrown", e);
					}
		        }
		    };
		    executor.submit(r1);
			
			
		}catch(Throwable e){
        	e.printStackTrace();
			Log.log(Level.FINEST, "An exception was thrown", e);
		}
	}
    

}
